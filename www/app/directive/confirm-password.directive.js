(function () {

  angular
    .module('app')
    .directive('compareTo', compareTo);

  function compareTo() {
    var directive = {
      require: 'ngModel',
      scope: {
        otherModelValue: '=compareTo',
      },
      link: function (scope, element, attributes, ngModel) {
        ngModel.$validators.compareTo = function (modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function () {
          ngModel.$validate();
        });
      },
    };

    return directive;
  }

})();
