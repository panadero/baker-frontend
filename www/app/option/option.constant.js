(function () {
  'use strict';

  angular
    .module('app.option')
    .constant('optionConstants', {
      DAYS_INFO: [{ name: 'Lun', value: 0 },
                  { name: 'Mar', value: 1 },
                  { name: 'Mie', value: 2 },
                  { name: 'Jue', value: 3 },
                  { name: 'Vie', value: 4 },
                  { name: 'Sab', value: 5 },
                  { name: 'Dom', value: 6 },],
      CURRENCY: '₵',
    });
})();
