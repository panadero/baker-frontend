(function () {
  'use strict';

  angular
    .module('app.option')
    .controller('OptionCtrl', OptionCtrl);

  /* @ngInject */
  function OptionCtrl($window,
                      $state,
                      optionConstants,
                      orderService,
                      personalOrdersService,
                      loginService,
                      optionService,
                      $ionicLoading,
                      $ionicPopup,
                      $ionicGesture) {
    var _this = this;
    _this.showAgain;
    _this.error = false;
    _this.title;
    _this.newOrder = true;
    _this.dayClick = dayClick;
    _this.dragLeft = dragLeft;
    _this.dragRight = dragRight;
    _this.released = released;
    _this.breadClick = breadClick;
    _this.checkOut = checkOut;
    _this.updateOrder = updateOrder;
    _this.logOut = logOut;
    _this.daysInfo = optionConstants.DAYS_INFO;
    _this.currency = optionConstants.CURRENCY;
    _this.breads = null;
    _this.selections;
    _this.backToNewOrder = backToNewOrder;
    var draged = [false, false];
    var errorCount = 0;
    var infoOnce = localStorage.getItem("showMessage");
    activate();

    function activate() {
      if (personalOrdersService.getBreadsObject().length > 0) {
        _this.title = 'Mis ordenes';
        _this.newOrder = false;
        _this.selections = personalOrdersService.getSelections();
        _this.breads = personalOrdersService.getBreadsObject();
      } else {
        $ionicLoading.show();
        _this.title = 'Pedidos';
        optionService.getBreads()
          .then(assignBreads)
          .catch(catchError);
      }

      if (infoOnce == null) {
        localStorage.setItem("showMessage", 'true')
        infoOnce = 'true';
      }
    }

    function tryAgain() {
      $ionicLoading.show();
      optionService.getBreads()
        .then(assignBreads)
        .catch(catchError);
    }

    function catchError() {
      $ionicLoading.hide();
      if (errorCount++ < 7) {
        tryAgain();
      }
    }

    function assignBreads(response) {
      $ionicLoading.hide();
      var names = [];
      var prices = [];
      var stocks = [];
      var breadIds = [];
      for (var bread in response.data) {
        var name = response.data[bread].breadType;
        var price = response.data[bread].price;
        var stock = response.data[bread].stock;
        var breadId = response.data[bread].id;
        if (name && price) {
          stocks[bread] = stock;
          names[bread] = name.toString().toLowerCase();
          prices[bread] = price;
          breadIds[bread] = breadId;
        }
      }

      var spanishNames = orderService.getSpanishNames(names);
      createBreadObject(names.length, names, prices, spanishNames, stocks, breadIds);
    }

    function checkOut() {
      var orderObject = {};
      var orderNumber = 0;
      for (var bread in _this.breads) {
        for (var day = 0; day < 7; day++) {
          if (_this.breads[bread][1][day].amount > 0) {
            orderObject[orderNumber++] = {
              type: _this.breads[bread][0],
              day: _this.daysInfo[day].name,
              price: _this.breads[bread][3],
              quantity: _this.breads[bread][1][day].amount,
              value: _this.daysInfo[day].value,
              stock: _this.breads[bread][5],
              breadId: _this.breads[bread][6],
            };
          }
        }
      }

      if (!orderObject[0]) {
        errorMessage();
      } else {
        orderService.setUserOrder(orderObject);
        $state.go('checkout');
      }
    }

    function updateOrder() {
      personalOrdersService.setDelete(true);
      $state.go('personal-orders');
    }

    function createBreadObject(numberOfBreads, breadNames, breadPrice, spanishNames, breadStocks, breadIds) {
      _this.breads = [numberOfBreads--];
      _this.selections = [];
      for (; numberOfBreads >= 0; numberOfBreads--) {
        _this.breads[numberOfBreads] = [breadNames[numberOfBreads],
            [{ name: 'L', value: 0, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'K', value: 1, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'M', value: 2, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'J', value: 3, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'V', value: 4, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'S', value: 5, clicked: false, buttonType: 'button-outline', amount: 0 },
             { name: 'D', value: 6, clicked: false, buttonType: 'button-outline', amount: 0 },],
            numberOfBreads,
            breadPrice[numberOfBreads],
            spanishNames[numberOfBreads],
            breadStocks[numberOfBreads],
            breadIds[numberOfBreads],];
        _this.selections[numberOfBreads] = false;
      }

      personalOrdersService.saveBreads(_this.breads);
    }

    function errorMessage() {
      $ionicPopup.alert({
        title: 'Por favor complete la información',
        template: 'Deslice su dedo hacia la derecha para agregar un pan y hacia la izquierda para quitar un pan',
        cssClass: 'text-center',
      });
    }

    function getInfo(user) {
      _this.error = false;
      var names = user.data.breadType;
      names = names.names;
      createBreadObject(names.length, names);
    }

    function noConnection() {
      var names = ['error'];
      _this.error = true;
      createBreadObject(1, names);
    }

    function logOut() {
      localStorage.removeItem('userSession');
      $state.go('home');
    }

    function breadClick(number) {
      _this.selections[number] = !_this.selections[number];
      if (localStorage.getItem('showMessage') == 'true' && infoOnce == 'true') {
        $ionicPopup.confirm({
          title: 'Informacion',
          template: '<style>img { width:100%; height:120%;}</style><img ng-src="img/info.png" alt="Image here">',
          cancelText: 'No volver a mostrar',
          cancelType: 'button-assertive',
          okText: 'Aceptar',
          okType: 'button-assertive',
        }).then(handleMessage);
        infoOnce = 'false';
      }
    }


    function handleMessage(data) {
      if (!data) {
        localStorage.setItem('showMessage', 'false')
      }
    }

    function dayClick(amount, day, breadNumber) {
      if (amount > 0) {
        _this.breads[breadNumber][1][day.value].buttonType = null;
      } else {
        _this.breads[breadNumber][1][day.value].buttonType = 'button-outline';
      }
    }

    function add(day, breadName, breadNumber) {
      dayClick(++_this.breads[breadNumber][1][day.value].amount, day, breadNumber);
    }

    function substract(day, breadName, breadNumber) {
      if (_this.breads[breadNumber][1][day.value].amount > 0) {
        dayClick(--_this.breads[breadNumber][1][day.value].amount, day, breadNumber);
      }
    }

    function dragLeft() {
      draged[0] = true;
    }

    function dragRight() {
      draged[1] = true;
    }

    function released(day, breadName, breadNumber) {
      if (draged[0]) {
        draged[0] = false;
        substract(day, breadName, breadNumber);

      } else if (draged[1]) {
        draged[1] = false;
        add(day, breadName, breadNumber);
      }
    }

    function backToNewOrder() {
      personalOrdersService.setBreadsObject([]);
      $state.reload();
    }
  }

})();
