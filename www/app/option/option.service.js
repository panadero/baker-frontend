(function () {
  'use strict';

  angular
    .module('app.option')
    .service('optionService', optionService);

  /* @ngInject */
  function optionService($http,
                         CORE) {
    var service = {
      url: CORE.API_URL,
      getBreads: getBreads,
      breadInfo: breadInfo,
    };

    return service;

    function getBreads() {
      var getRequest = {
        method: 'GET',
        url: service.url + 'bread',
        headers: {
          'Content-Type': 'application/json'
        },
      };
      return $http(getRequest);
    }

    function breadInfo(id) {
      var path = 'bread/'+id;
      var getRequest = {
        method: 'GET',
        url: service.url + 'bread/'+id,
        headers: {
          'Content-Type': 'application/json'
        },
      };
      return $http(getRequest);
    }
  }

})();
