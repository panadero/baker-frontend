(function () {
  'use strict';

  angular
    .module('app.option')
    .config(optionRoute)
    .run(runPush);


  function optionRoute($stateProvider) {
    $stateProvider
      .state('option', {
        url: '/option',
        templateUrl: 'app/option/option.html',
        controller: 'OptionCtrl',
        controllerAs: 'vm',
        cache: false,
      });
  }

  function runPush($ionicPlatform, sessionService) {
    $ionicPlatform.ready(function() {
      window.FirebasePlugin.getToken(function(token) {
        if (token != null) {
          sessionService.setCurrentToken(token);
        }
      }, function(error) {
        console.error(error);
      });

      window.FirebasePlugin.onTokenRefresh(function(token) {
        if (token != null) {
          sessionService.setCurrentToken(token);
        }
      }, function(error) {
          console.error(error);
      });
    });
  }

})();
