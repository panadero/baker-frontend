(function () {
  'use strict';

  angular
    .module('app.home')
    .controller('HomeCtrl', HomeCtrl);

  /* @ngInject */
  function HomeCtrl($state, sessionService) {
    var _this = this;
    _this.goToLogIn = goToLogIn;
    _this.goToCreateUser = goToCreateUser;

    var currentUser = sessionService.getCurrentUser();

    if (currentUser) {
      $state.go('option')
    }

    function goToCreateUser() {
      $state.go('client');
    }

    function goToLogIn() {
      $state.go('login');
    }

  }

})();
