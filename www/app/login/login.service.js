(function () {
  'use strict';

  angular
    .module('app.login')
    .service('loginService', loginService);

  /* @ngInject */
  function loginService($http,
                        sessionService,
                        CORE) {
    var service = {
      url: CORE.API_URL,
      logIn: LogIn,
      LogOut: LogOut,
      formatCredentials: formatCredentials,
    };

    return service;

    function LogIn(user) {
      var postRequest = {
        method: 'POST',
        url: service.url + 'login',
        headers: {
          'Content-Type': 'application/json'
        },
        data: service.formatCredentials(user),
      };
      return $http(postRequest);
    }

    function LogOut() {
    }

    function formatCredentials(user) {
      var json = {
        email: user.email,
        password: user.password,
      };
      return json;
    }

  }

})();
