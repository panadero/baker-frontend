(function () {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl($state,
                     $ionicPopup,
                     $ionicLoading,
                     $ionicNavBarDelegate,
                     paymentService,
                     loginService,
                     sessionService) {

    var _this = this;
    _this.authenticate = authenticate;
    _this.recover = recover;
    var userInfo;
    var loginAttemps = 0;

    activate();

    function activate() {
      $ionicNavBarDelegate.showBackButton(false);
    }

    function authSuccess(user) {
      $ionicLoading.hide();
      sessionService.setCurrentUser(userInfo);
      $state.go('option');
    }

    function authError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error de Autenticación',
        template: 'Autenticación Invalida',
        cssClass: 'text-center',
      });
    }

    function handleUserCredentials(user) {
      if (user.data.isApproved) {
        authSuccess(user.data);
      } else {
        authError();
      }
    }

    function apiLogOn(user) {
      if (user.data.fullName && user.data.password === userInfo.password + '#$') {
        userInfo = user.data;
        paymentService.logOn(user.data)
          .then(handleUserCredentials)
          .catch(authError);
      } else {
        authError();
      }
    }

    function authenticate(user) {
      loginAttemps = 0;
      $ionicLoading.show();
      if (angular.isDefined(user)) {
        user.email = user.email.toLowerCase();
        userInfo = user;
        tryLogin();
      } else {
        authError();
      }
    }

    function tryLogin() {
      if (loginAttemps < 5) {
        loginAttemps++;
        loginService.logIn(userInfo)
          .then(apiLogOn)
          .catch(tryLogin);
      } else {
        authError();
      }
    }

    function recover() {
      $ionicPopup.alert({
        title: 'Error de Ingreso',
        template: 'Vista no disponible',
        cssClass: 'text-center',
      });
    }

  }
})();
