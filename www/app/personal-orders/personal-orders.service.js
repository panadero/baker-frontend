(function () {
  'use strict';

  angular
    .module('app.personal-orders')
    .service('personalOrdersService', personalOrdersService);

  function personalOrdersService($http,
                                sessionService,
                                CORE) {
    var service = {
      url: CORE.API_URL,
      order: {},
      getOrder: getOrder,
      setOrder: setOrder,
      saveBreads: saveBreads,
      getBreads: getBreads,
      breads: [],
      getBreadOrders: getBreadOrders,
      setBreadsObject: setBreadsObject,
      getBreadsObject: getBreadsObject,
      breadsObject: [],
      getSelections: getSelections,
      setSelections: setSelections,
      selections: [],
      setDelete: setDelete,
      getDelete: getDelete,
      deleteOrders: false,
    };
    return service;

    function setDelete(doDelete) {
      service.deleteOrders = doDelete;
    }

    function getDelete() {
      return service.deleteOrders;
    }

    function getOrder() {
      return service.order;
    }

    function setOrder(order) {
      service.order = order;
    }

    function saveBreads(breads) {
      service.breads = breads;
    }

    function getBreads() {
      return service.breads;
    }

    function getBreadOrders() {
      var headers = { "Authorization": sessionService.getAuthorization };
      var json = {
        method: 'GET',
        url: service.url + 'breadorder',
        headers: headers,
      };
      return $http(json);
    }

    function setBreadsObject(object) {
      service.breadsObject = object;
    }

    function getBreadsObject() {
      return service.breadsObject;
    }

    function setSelections(selections) {
      service.selections = selections;
    }

    function getSelections() {
      return service.selections;
    }
  }
})();
