(function () {
  'use strict';

  angular
    .module('app.personal-orders')
    .controller('PersonalOrdersController', PersonalOrdersController);

  function PersonalOrdersController($state,
                                    personalOrdersService,
                                    orderService,
                                    paymentService,
                                    optionConstants,
                                    $ionicPopup,
                                    $ionicLoading) {
    var _this = this;
    _this.breads = personalOrdersService.getBreads();
    _this.orders = personalOrdersService.getOrder();
    var selections = [];
    var myBreadOrders = [];
    var ordersIds = {};
    var fails = 0;
    var totalOrders = 0;
    var usedTokens = '';
    activate();

    function activate() {
      if (personalOrdersService.getDelete()) {
        totalOrders = _this.orders.length;
        deleteOrders(Object.keys(_this.orders)[0]);
      } else {
        getBreadOrders();
      }
    }

    function deleteOrders(current) {
      if (current != totalOrders) {
        orderService.cancelOrder(_this.orders[current])
          .then(cancelFTOrder(_this.orders[current].chargeToken, _this.orders[current].client.email, current))
          .catch(function () {
            tryAgain(current);
          });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Orden Eliminada',
          template: 'Hemos eliminado su orden con exito',
          cssClass: 'text-center',
        });
        alertPopup.then(function(res) {
          personalOrdersService.setDelete(false);
          $state.go('profile');
          location.reload();
        });
      }
    }

    function cancelFTOrder(token, user, current) {
      if (!usedTokens.includes(token)) {
        usedTokens += token;
        if(!token) {
          deleteOrders(++current);
        } else {
          paymentService.deleteCharge(token, user)
            .then(function (response) {
              handleFTCancel(response, current);
            });
        }
      } else {
        deleteOrders(++current);
      }
    }

    function handleFTCancel(response, current) {
      if (response.data.isApproved) {
        deleteOrders(++current);
      }
    }

    function tryAgain(current) {
      if (fails++ < 11) {
        deleteOrders(current);
      } else if (fails == 13) {
        $ionicPopup.alert({
          title: 'No podemos eliminar su orden',
          template: 'Por favor intente de nuevo',
          cssClass: 'text-center',
        });
      }
    }

    function prepareBreadObject(response) {
      var breadOrders = response.data;
      var breadOrderCount = 0;
      for (var clean in _this.breads) {
        for (var day = 0; day < 7; day++) {
          _this.breads[clean][1][day].amount = 0;
          _this.breads[clean][1][day].buttonType = 'button-outline';
          _this.breads[clean][1][day].clicked = false;
        }
      }

      for (var myBreadOrder in breadOrders) {
        selections[myBreadOrder] = true;
        for (var bread in _this.breads) {
          if (breadOrders[myBreadOrder].bread.id == _this.breads[bread][6]) {
            for (var myOrder in _this.orders) {
              var dayNumber = getDayNumber(_this.orders[myOrder].day);
              if (breadOrders[myBreadOrder].order.id == _this.orders[myOrder].id) {
                _this.breads[bread][1][dayNumber].amount += breadOrders[myBreadOrder].amount;
                _this.breads[bread][1][dayNumber].buttonType = null;
                _this.breads[bread][1][dayNumber].clicked = true;
              }
            }
          }
        }
      }

      personalOrdersService.setSelections(selections);
      personalOrdersService.setBreadsObject(_this.breads);
      $state.go('option');
    }

    function getBreadOrders() {
      $ionicLoading.show({
        hideOnStateChange: true,
      });
      personalOrdersService.getBreadOrders()
        .then(prepareBreadObject)
        .catch(handleError);
    }

    function handleError() {
      $ionicPopup.alert({
        title: 'Error cargando sus ordenes',
        template: 'Intente nuevamente en unos instantes',
      });
      $state.go('profile');
    }

    function getDayNumber(day) {
      switch (day) {
        case 'Lunes':
          return 0;
        case 'Martes':
          return 1;
        case 'Miércoles':
          return 2;
        case 'Jueves':
          return 3;
        case 'Viernes':
          return 4;
        case 'Sábado':
          return 5;
        default:
          return 6;
      }
    }

  }
})();
