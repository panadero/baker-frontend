(function () {
  'use strict';

  angular
    .module('app.personal-orders')
    .config(personalOrdersRoute);

  function personalOrdersRoute($stateProvider) {
    $stateProvider
      .state('personal-orders', {
        url: '/personal-orders',
        templateUrl: 'app/personal-orders/personal-orders.html',
        controller: 'PersonalOrdersController',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
