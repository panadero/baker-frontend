(function () {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.login',
      'app.option',
      'app.check',
      'app.client',
      'app.home',
      'app.profile',
      'app.card',
      'app.personal-orders',
    ]);
})();
