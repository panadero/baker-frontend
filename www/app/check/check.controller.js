(function () {
  angular
    .module('app.check')
    .controller('CheckoutCtrl', CheckoutCtrl);

  /* @ngInject */
  function CheckoutCtrl($location,
                        orderService,
                        sessionService,
                        paymentService,
                        $ionicPopup,
                        $ionicLoading,
                        $state,
                        $scope) {

    var _this = this;
    _this.orders;
    _this.plural = plural;
    _this.totalPrice;
    _this.spanishName = spanishName;
    _this.delivery = 550;
    _this.subtotal = getSubTotalPrice;
    _this.editOrder = editOrder;
    _this.orderType = orderType;
    var user = sessionService.getCurrentUser();
    var clientInfo;
    var chargeTokenId;
    var dayOrderNumber = [];
    var orderMode;
    var failedAttempts = 0;
    var orderNumber = 0;
    var successMessage;
    var breadOrderDay;
    var cardToken;
    var userName;
    var totalOrders = 0;
    var orderList = [];
    var subOrder;
    activate();

    function activate() {
      orderService.getClient().then(getClientInfo);
      _this.orders = completeDayName(sortOrders(orderService.userOrder));
      var counter = 0;
      for (day in _this.orders) {
        totalOrders += _this.orders[day].length;
        dayOrderNumber[counter++] = day;
      }

      successMessage = false;
      _this.totalPrice = getTotalPrice();
      paymentService.logOnApp();
    }

    function editOrder() {
      $state.go('option');
    }

    function orderType() {
      $scope.data = { uniqueOrder: false, regularOrder: false };
      $ionicPopup.show({
        title: 'Seleccione el tipo de orden',
        cssClass: 'text-center',
        templateUrl: 'app/check/orderType.html',
        scope: $scope,
        buttons: [ {
          text: 'Aceptar',
          type: 'button-assertive',
        } ],
      }).then(function () {
        if ($scope.data.uniqueOrder) {
          orderMode = ['Pedido Especial', 2];
          verifyCards();
        } else if ($scope.data.regularOrder) {
          orderMode = ['Regular', 1];
          verifyCards();
        } else {
          orderType();
        }
      });
    }

    function verifyCards() {
      $ionicLoading.show();
      paymentService.cards(user)
        .then(handleCard)
        .catch(handleCard);
    }

    function handleCard(response) {
      if (response.data.isAvailableCard) {
        cardToken = response.data.userCards[0].cardTokenId;
        userName = response.data.userName;
        addNormalOrders(0);
      } else {
        $ionicLoading.hide();
        message('Información de Pago',
                'Por favor agregue su tarjeta para completar su orden',
                'card');
      }
    }

    function addNormalOrders(orderNumber) {
      partialPrice = getPartialPrice(orderNumber);
      paymentService.includeCharge(userName, partialPrice)
        .then(function (response) {
          if (response.data.isApproved) {
            chargeTokenId = response.data.chargeTokenId;
            orderService.submitOrder(prepareJSON(orderService.userOrder[orderNumber]), clientInfo, chargeTokenId, cardToken)
              .then(function (response) {
                orderList.push(response.data);
                if (++orderNumber < Object.keys(orderService.userOrder).length){
                  addNormalOrders(orderNumber);
                } else {
                  failedAttempts = 0;
                  addBreadOrders(0);
                }
              })
              .catch(function () {
                if (++failedAttempts < 7) {
                  addNormalOrders(orderNumber);
                } else {
                  $ionicLoading.hide();
                  message('Información de Pago',
                          'No pudimos realizar su pedido, intente de nuevo en unos segundos',
                          false);
                }
              });
          }
        });
    }

    function getPartialPrice(orderNumber) {
      ordersQantity = getOrderQuantityForDay(orderService.userOrder[orderNumber].day)
      price = orderService.userOrder[orderNumber].price
      quantity = orderService.userOrder[orderNumber].quantity
      return parseInt(price * quantity) + parseInt(Math.round(_this.delivery / ordersQantity));
    }

    function getOrderQuantityForDay(day) {
      ordersByDay = 0;
      for(orderItem in orderService.userOrder) {
        if (orderService.userOrder[orderItem].day == day) {
          ordersByDay++;
        }
      }
      if (ordersByDay === 0) {
        return 1;
      } else {
        return ordersByDay;
      }
    }

    function addBreadOrders(orderNumber) {
      orderService.submitBreadOrder(prepareBreadJSON(orderService.userOrder[orderNumber], orderNumber))
        .then(function () {
          if (++orderNumber < Object.keys(orderService.userOrder).length) {
            addBreadOrders(orderNumber);
          } else {
            $ionicLoading.hide();
            message('Orden Procesada',
                    'Hemos procesado su orden, se le cobrará hasta que reciba el producto',
                    'option');
          }
        })
        .catch(function () {
          if (++failedAttempts < 7) {
            addBreadOrders(orderNumber);
          } else {
            $ionicLoading.hide();
            message('Información de Pago',
                    'No pudimos realizar su pedido, intente de nuevo en unos segundos',
                    false);
          }
        })
    }


    function prepareJSON(order) {
      var json = {
        day: order.day,
        date: createDateObject(order.value),
        type: orderMode,
        pushToken: sessionService.getCurrentToken(),
      };

      return json;
    }

    function prepareBreadJSON(order, orderNumber) {
      var breadJSON = {
        amount: order.quantity,
        price: getPartialPrice(orderNumber),
        bread: {id: order.breadId,
                breadType: order.type,
                stock: order.stock,
                price: order.price.price,},
        order: orderList[orderNumber],
      };
      return breadJSON;
    }

    function getClientInfo(response) {
      var user = sessionService.getCurrentUser();
      var userInfo;
      for (client in response.data) {
        if (response.data[client].email == user.email) {
          userInfo  = response.data[client];
          break;
        }
      }

      clientInfo =  userInfo;
    }

    function getTotalPrice() {
      var total = 0;
      var deliveryDays = 0;
      for (day in _this.orders) {
        for (order in _this.orders[day]) {
          total += _this.orders[day][order].price * _this.orders[day][order].quantity;
        }
        deliveryDays++;
      }
      return total + _this.delivery * deliveryDays;
    }

    function getSubTotalPrice(day) {
      var subtotal = 0;
      for (order in day) {
        subtotal += day[order].price * day[order].quantity;
      }

      return subtotal + _this.delivery;
    }

    function completeDayName(orders) {
      var dayName;
      for (delivery in orders) {
        for (order in orders[delivery]) {
          switch (orders[delivery][order].day) {
            case 'Lun':
              dayName ='Lunes';
            break;
            case 'Mar':
              dayName = 'Martes';
              break;
            case 'Mie':
              dayName = 'Miércoles';
              break
            case 'Jue':
              dayName = 'Jueves';
            break;
            case 'Vie':
              dayName = 'Viernes';
              break;
            case 'Sab':
              dayName = 'Sábado'
              break;
            case 'Dom':
              dayName = 'Domingo';
          }

          orders[delivery][order].day = dayName;
        }
      }

      return orders;
    }

    function sortOrders(allOrders) {
      var dayOrders = [0, 0, 0, 0, 0, 0, 0];
      var finalOrder = {};
      for (var order in allOrders) {
        dayOrders[allOrders[order].value]++;
        finalOrder[allOrders[order].value] = [];
      }

      for (var order in allOrders) {
        finalOrder[allOrders[order].value][--dayOrders[allOrders[order].value]] = allOrders[order];
      }

      return finalOrder;
    }

    function plural(amount) {
      if (amount > 1) {
        return 'es';
      }

      return '';
    }

    function spanishName(name) {
      return orderService.getSpanishNames([name]).toString();
    }

    function createDateObject(deliveryDay) {
      var date = new Date();
      var targetDay;

      if ((++deliveryDay <= date.getDay()) || (deliveryDay == 7 && date.getDay() == 0)) {
        if (deliveryDay == 7) {
          deliveryDay = 0;
        }
        targetDay = date.getDate() + (6 - date.getDay()) + ++deliveryDay;
        date.setDate(targetDay);
      } else {
        var tomorrow = new Date(date.getTime() + 24 * 60 * 60 * 1000);
        var numberOfDays = 0;
        var tomorrowDeliveryDay;

        if (deliveryDay > 6) {
          tomorrowDeliveryDay = 0;
        } else {
          tomorrowDeliveryDay = deliveryDay;
        }

        if ((date.getHours < 18) && (tomorrowDeliveryDay == tomorrow.getDay())) {
          numberOfDays = 7;
        }

        targetDay = date.getDate() + Math.abs((deliveryDay + numberOfDays - date.getDay()));
        date.setDate(targetDay);
      }

      return date;
    }

    function message(titleMessage, info, state) {
        $ionicPopup.alert({
        title: titleMessage,
        template: info,
        cssClass: 'text-center',
        buttons: [ {
          text: '<b>Aceptar</b>',
          type: 'button-assertive',
          onTap: function() {
            if (state) {
              $state.go(state);
            }
          },
        }],
      });
    }

  }

})();
