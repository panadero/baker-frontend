(function () {
  'use strict';

  angular
    .module('app.check')
    .config(checkRoute);

  function checkRoute($stateProvider) {

    $stateProvider
      .state('checkout', {
        url: '/check',
        templateUrl: 'app/check/check.html',
        controller: 'CheckoutCtrl',
        controllerAs: 'vm',
        cache: false
      });
  }
})();
