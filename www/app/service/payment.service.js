(function () {
  'use strict'

  angular
    .module('app')
    .service('paymentService', paymentService);

  function paymentService($http, CORE) {
    var service = {
      createUser: createUser,
      logOn: logOn,
      addCard: addCard,
      updateCard: updateCard,
      deleteCard: deleteCard,
      logOnApp: logOnApp,
      includeCharge: includeCharge,
      applyTransaction: applyTransaction,
      defaultCard: defaultCard,
      cards: cards,
      deleteCharge: deleteCharge,
      deleteUser: deleteUser
    };
    return service;
    function doPost(request) {
      return $http(request);
    }

    function createUser(userInfo) {
      var name = userInfo.fullName.split(' ');
      var createUserPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/createuser',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: userInfo.email,
                userFirstName: name[0],
                userLastName: name[name.length - 1],
                userPassword: userInfo.password,
                userEmail: userInfo.email,
                userCallerId: userInfo.phone, },
      };
      return doPost(createUserPost);
    }

    function logOn(credentials) {
      var logOnPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/logonuser',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: credentials.email,
                userPassword: credentials.password,
              },
      };

      return doPost(logOnPost);
    }

    function logOnApp() {
      var logOnPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/LogOnApp',
        data: { applicationName: CORE.APPLICATION_NAME,
                applicationPassword: CORE.APPLICATION_PASSWORD, },
      };
      return doPost(logOnPost);
    }

    function addCard(card) {
      var addCardPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/UserIncludeCard',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: card.username,
                userPassword: card.passWord,
                cardDescription: card.cardType,
                primaryAccountNumber: card.number,
                expirationMonth: card.expirationMonth,
                expirationYear: card.expirationYear,
                verificationValue: card.code,
              },
      };

      var result = doPost(addCardPost);
      return result;
    }

    function updateCard(card) {
      var updateCardPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/UserUpdateCard',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: card.username,
                userPassword: card.passWord,
                cardTokenId: card.token,
                cardDescription: card.cardType,
                primaryAccountNumber: card.number,
                expirationMonth: card.experiationMonth,
                expirationYear: card.expirationYear,
                verificationValue: card.code,
              },
      };
      return doPost(updateCardPost);
    }

    function deleteCard(card) {
      var deleteCardPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/UserDeleteCard',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: card.username,
                userPassWord: card.passWord,
                cardTokenId: card.token,
              },
      };
      return doPost(deleteCardPost);
    }

    function includeCharge(username, amount) {
      var transactionPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppIncludeCharge',
        data: { applicationName: CORE.APPLICATION_NAME,
                applicationPassword: CORE.APPLICATION_PASSWORD,
                chargeDescription: CORE.CHARGE_DESCRIPTION,
                userName: username,
                transactionCurrency: CORE.CURRENCY,
                transactionAmount: amount, },
      };
      return doPost(transactionPost);
    }

    function applyTransaction(transactionInfo) {
      var transactionPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppIncludeCharge',
        data: { applicationName: CORE.APPLICATION_NAME,
                applicationPassword: CORE.APPLICATION_PASSWORD,
                userName: transaction.username,
                chargeTokenId: CORE.CURRENCY,
                cardTokenId: transaction.amount, },
      };
    }

    function cards(user) {
      var cardInfo = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/UserRequestCards',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: user.email,
                userPassword: user.password, },
      };
      return doPost(cardInfo);
    }

    function defaultCard(user) {
      var jsonInfo = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppVerifyUserDefaultCard',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: user,
                applicationPassword: CORE.APPLICATION_PASSWORD, },
      };
      return doPost(jsonInfo);
    }

    function deleteCharge(token, user) {
      var JSON = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppDeleteCharge',
        data: { applicationName: CORE.APPLICATION_NAME,
                userName: user,
                applicationPassword: CORE.APPLICATION_PASSWORD,
                chargeTokenId: token, },
      };
      return doPost(JSON);
    }

    function deleteUser(user) {
      var JSON = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/UserDeleteProfile',
        data: {
          applicationName: CORE.APPLICATION_NAME,
          userName: user.email,
          userEmail: user.email,
          userPassword: user.password
        },
      };
      return doPost(JSON);
    }
  }
})();
