(function () {
  'use strict';

  angular
    .module('app')
    .service('sessionService', sessionService);

  /* @ngInject */
  function sessionService($base64) {
    var service = {
      getAuthorization: getAuthorization,
      setCurrentUser: setCurrentUser,
      getCurrentUser: getCurrentUser,
      setCurrentToken: setCurrentToken,
      getCurrentToken: getCurrentToken,
    };

    return service;

    function getAuthorization() {
      var user = getCurrentUser();
      return 'Basic ' + $base64.encode(user.email + ':' + user.password);
    }

    function setCurrentUser(user) {
      localStorage.setItem('userSession', JSON.stringify(user));
    }

    function getCurrentUser() {
      return JSON.parse(localStorage.getItem('userSession'));
    }

    function setCurrentToken(token) {
      localStorage.setItem('pushToken', token);
    }

    function getCurrentToken() {
      return localStorage.getItem('pushToken');
    }
  }

})();
