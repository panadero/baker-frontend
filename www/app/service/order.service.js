(function () {
  'use strict';

  angular
    .module('app')
    .service('orderService', orderService);

  /* ngInject */
  function orderService($http, CORE, sessionService) {
    var service = {
      url: CORE.API_URL,
      userOrder: {},
      getSpanishNames: getSpanishNames,
      setUserOrder: setUserOrder,
      submitOrder: submitOrder,
      submitBreadOrder: submitBreadOrder,
      getClient: getClient,
      getOrders: getOrders,
      cancelOrder: cancelOrder,
    };

    return service;

    function getSpanishNames(enNames) {
      var spanishNames = [enNames.length];
      for (var name = 0; name < enNames.length; name++) {
        switch (enNames[name]) {
          case 'ciabbata':
            spanishNames[name] = 'Ciabbata';
            break;
          case 'italian':
            spanishNames[name] = 'Italiano';
            break;
          case 'wheat':
            spanishNames[name] = 'Integral';
            break;
          case 'cheese':
            spanishNames[name] = 'Baguette con queso';
            break;
          case 'spanish':
            spanishNames[name] = 'Español';
            break;
          case 'french':
            spanishNames[name] = 'Frances';
            break;
          case 'sesame':
            spanishNames[name] = 'Baguette con ajonjolí';
            break;
          case 'baguette':
            spanishNames[name] = 'Baguette';
            break;
          case 'big_roll':
            spanishNames[name] = 'Mano Grande';
          break;
        }
      }

      return spanishNames;
    }

    function setUserOrder(order) {
      service.userOrder = order;
    }

    function submitBreadOrder(order) {
      var headers = { "Authorization": sessionService.getAuthorization };
      var json = {
        method: 'POST',
        url: service.url + 'breadorder',
        headers: {
          'Content-Type': 'application/json',
        },
        data: {
          amount: order.amount,
          price: order.price,
          bread: order.bread,
          order: order.order,
        },
        headers: headers,
      };
      return $http(json);
    }

    function submitOrder(orderInfo, currentClient, chargeToken, cardToken) {
      var headers = { "Authorization": sessionService.getAuthorization };
      var orderJson = {
        method: 'POST',
        url: service.url + 'order',
        data: {
          deliveryDate: orderInfo.date,
          state: 0,
          day: orderInfo.day,
          client: currentClient,
          orderType: { orderType: orderInfo.type[0],
                      id: orderInfo.type[1], },
          pushToken: orderInfo.pushToken,
          chargeToken: chargeToken,
          cardToken: cardToken,
        },
        headers: headers,
      };
      return $http(orderJson);
    }

    function getClient() {
      var clientJSON = {
        method: 'GET',
        url: service.url + 'client',
      };

      return $http(clientJSON);
    }

    function getOrders() {
      var json = {
        method: 'GET',
        url: service.url + 'order',
        headers: {
          'Content-Type': 'application/json',
        },
      };
      return $http(json);
    }

    function cancelOrder(order) {
      order.orderType.id = 3;
      order.orderType.orderType = 'Cancelado';
      var json = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        url: service.url + 'order',
        data: prepareOrderJSON(order),
      };

      return $http(json);
    }

    function prepareOrderJSON(order) {
      var json = {
        client: order.client,
        day: order.day,
        deliveryDate: order.deliveryDate,
        id: order.id,
        orderType: order.orderType,
        state: false,
      };
      return json;
    }
  }

})();
