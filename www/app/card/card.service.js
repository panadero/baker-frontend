(function () {
  'use strict';

  angular
    .module('app.card')
    .service('cardService', cardService);

  function cardService() {
    var service = {
      getCardType: getCardType,
      currentCard: currentCard,
      getOldCard: getOldCard,
    };
    var cardNumber;
    return service;

    function getCardType(card) {
      cardNumber = card;
      var card = {
        amex: [
          ['34'],
          ['37'],
        ],
        diners: [
          ['300', '305'],
          ['309'],
          ['36'],
          ['38', '39'],
        ],
        dinersUS: ['54', '55'],
        discover: [
          ['6011'],
          ['622126', '622925'],
          ['644', '649'],
          ['65'],
        ],
        masterCard: ['50', '55'],
        visa: ['4'],
        visaElectron: [
          ['4026'],
          ['417500'],
          ['4405'],
          ['4508'],
          ['4844'],
          ['4913'],
          ['4917'],
        ],
      };

      if (cardType(card.amex)) {
        return 'American Express';
      } else if (cardType(card.diners)) {
        return 'Diners';
      } else if (cardType(card.dinersUS)) {
        return 'DinersUS';
      } else if (cardType(card.discover)) {
        return 'Discover';
      } else if (cardType(card.masterCard)) {
        return 'Master Card';
      } else if (cardType(card.visa)) {
        return 'Visa';
      } else if (cardType(card.visaElectron)) {
        return 'Visa Electron';
      } else {
        return '';
      }
    }

    function cardType(card) {
      try {
        for (var pre = 0; pre < card.length; pre++) {
          if (card[pre].length == 1 && cardNumber.toString().startsWith(card[pre])) {
            return true;
          } else if (card[pre].length == 2) {
            var prefixLength = 0;
            while (card[pre][0].charAt(prefixLength++) != '');
            var actualPrefix = cardNumber.toString().substring(0, --prefixLength);
            if (parseInt(actualPrefix) > parseInt(card[pre][0])
                && parseInt(actualPrefix) < parseInt(card[pre][1])) {
              return true;
            }
          }
        }

        return false;
      } catch (error) {}
    }

    function currentCard(card) {
      sessionStorage.setItem('oldCard', JSON.stringify(card));
    }

    function getOldCard() {
      return JSON.parse(sessionStorage.getItem('oldCard'));
    }
  }
})();
