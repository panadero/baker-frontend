(function () {
  'use strict';

  angular
    .module('app.card')
    .config(cardRoutes);

  function cardRoutes($stateProvider) {
    $stateProvider
      .state('card', {
        url: '/card',
        templateUrl: 'app/card/card.html',
        controller: 'CardController',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
