(function () {
  'use strict';

  angular
    .module('app.card')
    .controller('CardController', CardController);

  function CardController(sessionService,
                          paymentService,
                          cardService,
                          $ionicPopup) {
    var _this = this;
    _this.addCard;
    _this.addUpdateMessage;
    _this.getCardType = getCardType;
    _this.cardCarrier;
    _this.deleteButton = false;
    _this.deleteMessage;
    _this.deleteCard = deleteCard;
    var card = cardService.getOldCard();
    var user = sessionService.getCurrentUser();
    activate();

    function activate() {
      if (card) {
        _this.addUpdateMessage = 'Editar mi método de pago';
        _this.deleteMessage = 'Eliminar mi tarjeta ' + card.userCards[0].cardMasked;
        _this.deleteButton = true;
        _this.addCard = changeCard;
      } else {
        _this.addUpdateMessage = 'Agregar método de pago';
        _this.addCard = addCard;
      }
    }

    function addCard() {
      var cardJSON = {
        username: user.email,
        passWord: user.password,
        cardType: _this.cardCarrier,
        number: _this.card.number,
        expirationYear: getYear(),
        expirationMonth: getMonth(),
        code: _this.card.code,
      };
      
      paymentService.addCard(cardJSON)
        .then(handleAddCard);
    }

    function changeCard() {
      var json = {
        username: user.email,
        passWord: user.password,
        token: card.userCards[0].cardTokenId,
        cardType: _this.cardCarrier,
        number: _this.card.number,
        expirationYear: card.userCards[0].expirationYear,
        expirationMonth: card.userCards[0].expirationMonth,
        code: _this.card.code,
      };
      paymentService.updateCard(json)
        .then(handleUpdate);
    }

    function deleteCard() {
      var json = {
        username: user.email,
        passWord: user.password,
        token: card.userCards[0].cardTokenId,
      };
      paymentService.deleteCard(json)
        .then(handleDelete)
        .catch(handleError);
    }

    function handleAddCard(result) {
      if (result.data.isApproved) {
        message('Método de pago agregado', 'Hemos guardado sus nuevos datos');
      } else {
        handleError(result.data);
      }
    }

    function handleUpdate(result) {
      if (result.data.isApproved) {
        message('Método de pago actualizado', 'Hemos actualizado sus datos de pago');
      } else {
        handleError(result.data);
      }
    }

    function handleDelete(result) {
      if (result.data.isApproved) {
        message('Tarjeta eliminada', 'Eliminamos su tarjeta de nuestro sistema');
        cardService.currentCard(null);
      } else {
        handleError(result.data);
      }
    }

    function handleError(error) {
      console.log(error);
      message('Error de información', error.apiStatus);
    }

    function getYear() {
      return _this.card.date.toString().split(' ')[3];
    }

    function getMonth() {
      var date = _this.card.date;
      return date.getMonth() + 1;
    }

    function getCardType() {
      _this.cardCarrier = cardService.getCardType(_this.card.number);
    }

    function message(messageTitle, message) {
      $ionicPopup.alert({
        title: messageTitle,
        template: message,
        cssClass: 'text-center',
      });
    }

    function cardError() {
      $ionicPopup.alert({
        title: 'No se ha agregado la tarjeta',
        template: 'No pudimos agregar esta tarjeta\nPor favor, intente de nuevo en unos minutos',
        cssClass: 'text-center',
      });
    }
  }

})();
