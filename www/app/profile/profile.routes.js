(function () {
  'use strict';

  angular
    .module('app.profile')
    .config(profileRoutes);

  function profileRoutes($stateProvider) {
    $stateProvider
      .state('profile', {
        url: '/profile',
        templateUrl: 'app/profile/profile.html',
        controller: 'ProfileController',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
