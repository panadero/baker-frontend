(function () {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileController', ProfileController);

  function ProfileController($state,
                             sessionService,
                             orderService,
                             personalOrdersService,
                             cardService,
                             $ionicPopup,
                             $ionicLoading,
                             paymentService) {
    var _this = this;
    _this.user = sessionService.getCurrentUser();
    personalOrdersService.setBreadsObject([]);
    _this.card;
    _this.cardImage;
    _this.addCard = addCard;
    _this.cardFound = false;
    _this.getMyOrders = getMyOrders;
    _this.backToOptions = backToOptions;
    var orderType;
    getCard();

    function backToOptions() {
      personalOrdersService.setSelections([]);
      $state.go('option');
    }

    function getCard() {
      paymentService.cards(_this.user)
        .then(setCard);
    }

    function setCard(card) {
      if (card.data.isAvailableCard) {
        setCardImage(card.data);
        _this.card = card.data.userCards[0].cardMasked;
        _this.cardFound = true;
        cardService.currentCard(card.data);
      } else {
        _this.card = 'Agregue una tarjeta a su cuenta';
      }
    }

    function addCard() {
      $state.go('card');
    }

    function noInternet() {
      _this.card = 'No tiene conexión a Internet';
    }

    function setCardImage(card) {
      switch (cardService.getCardType(card.userCards[0].cardMasked.toString())) {
        case 'American Express':
          _this.cardImage = 'img/amex.png';
        break;
        case 'Diners':
          _this.cardImage = 'img/diners.png';
        break;
        case 'DinersUS':
          _this.cardImage = 'img/diners.png';
        break;
        case 'Discover':
          _this.cardImage = 'img/discover.png';
        break;
        case 'Master Card':
          _this.cardImage = 'img/master.png';
        break;
        case 'Visa':
          _this.cardImage = 'img/visa.png';
        break;
        case 'Visa Electron':
          _this.cardImage = 'img/visae.png';
        break;
      }
    }

    function getMyOrders(type) {
      $ionicLoading.show();
      orderType = type;
      orderService.getOrders()
        .then(prepareOrders)
        .catch(handleError);
    }

    function prepareOrders(allOrders) {
      $ionicLoading.hide();
      var myOrders = [];
      var searchFor = 2;
      if (orderType) {
        searchFor = 1;
      }

      var today = new Date();
      for (var order in allOrders.data) {
        var viewOrder = allOrders.data[order];
        if (viewOrder.client.id == _this.user.id && viewOrder.orderType.id == searchFor) {
          var date = viewOrder.deliveryDate;
          var orderDate = new Date(date.substring(0, 4), date.substring(5, 7) - 1, date.substring(8, 10));
          if (searchFor == 1 || (searchFor == 2 && today <= orderDate && viewOrder.state != true)) {
            myOrders[order] = viewOrder;
          }
        }
      }

      if (myOrders.length) {
        personalOrdersService.setOrder(myOrders);
        $state.go('personal-orders');
      } else {
        $ionicPopup.alert({
          title: 'No hay ordenes activas',
          template: 'Agregue ordenes a su cuenta',
          cssClass: 'center',
        });
      }
    }

    function handleError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error cargando sus ordenes',
        template: 'Intente nuevamente en unos instantes',
      });
    }
  }
})();
