(function () {
  'use strict';

  var DEV_PATH = 'http://localhost:8080/rest/';
  var STAGING_PATH = 'https://baker-staging.herokuapp.com/rest/';
  var PRODUCTION_PATH = 'https://baker-production.herokuapp.com/rest/';
  var DEV_PAYMENT_PATH = 'http://www.fttserver.com:4217';
  var PRODUCTION_PAYMENT_PATH = 'https://www.fttserver.com:2153';
  var DEV_APP_NAME = 'PERNIX_DEV';
  var DEV_APP_PASSWORD = 'PernixDev2017%';
  var PRODUCTION_APP_NAME = 'PANADERO';
  var PRODUCTION_APP_PASSWORD = 'Panadero2017%';

  angular
    .module('app.core')
    .constant('CORE', {
      'API_URL': DEV_PATH,
      'APPLICATION_NAME': DEV_APP_NAME,
      'APPLICATION_PASSWORD': DEV_APP_PASSWORD,
      'CURRENCY': 'CRC',
      'CHARGE_DESCRIPTION': 'Servicio de pan express',
      'PAYMENT_SERVICE_URL': DEV_PAYMENT_PATH
    });
})();
