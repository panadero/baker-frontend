(function () {
  'use strict';

  angular
    .module('app.client')
    .controller('ClientCtrl', ClientCtrl);

  /* @ngInject */
  function ClientCtrl($state,
                      $ionicPopup,
                      $ionicLoading,
                      clientService,
                      paymentService) {

    var _this = this;
    _this.save = save;
    var clientBuilt;
    var retries = 0;
    _this.client = { address: "Colinas del Sol" };

    function save(client) {
      retries = 0;
      if (client.password === client.confirmPassword) {
        if (angular.isDefined(client)) {
          $ionicLoading.show();
          clientBuilt = buildClient(client);
          paymentService.createUser(clientBuilt)
            .then(createUser)
            .catch(authError);
        } else {
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Error en el Registro',
            template: 'Verifique la información a ingresar',
            cssClass: 'text-center',
          });
        }
      } else {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error en el Registro',
          template: 'Contraseñas no son iguales',
          cssClass: 'text-center',
        });
      }
    }

    function createUser(response) {
      if (response.data.isApproved) {
        tryCreateUser();
      } else if (response.data.apiStatus == 'Invalid Mail Format') {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error en el Registro',
          template: 'Por favor ingrese un correo valido',
          cssClass: 'text-center',
        });
      } else if (response.data.apiStatus == 'InvalidPassword') {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error en el Registro',
          template: 'Por favor ingrese una contraseña más larga',
          cssClass: 'text-center',
        });
      } else if (response.data.apiStatus == 'DuplicateUserName') {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error en el Registro',
          template: 'Ese correo ya está registrado',
          cssClass: 'text-center',
        });
      } else {
        authError(response);
      }
    }

    function buildClient(client) {
      var json = {
        email: client.email.toLowerCase(),
        password: client.password + '#$',
        fullName: client.fullName,
        phone: client.phone,
        city: client.city,
        address: client.address,
        houseNumber: client.houseNumber,
      };
      return json;
    }

    function tryCreateUser() {
      if (retries < 5) {
        retries++;
        clientService.createClient(clientBuilt)
          .then(authSuccess)
          .catch(tryCreateUser);
      } else {
        paymentService.deleteUser(clientBuilt)
          .then(function(response) {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: 'Hay problemas registrando el usuario. Por favor intente mas tarde.',
              template: 'Error registrando el usuario',
              cssClass: 'text-center',
            });
          })
          .catch(function(response) {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: 'Hay problemas registrando el usuario. No se pudo eliminar el usuario del servicio de pago!.',
              template: 'Error eliminando el usuario del servicio de pago',
              cssClass: 'text-center',
            });
          });
      }
    }

    function authSuccess() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Notificación',
        template: 'Usuario Creado!',
        cssClass: 'text-center',
      }).then(goToLogin);
    }

    function goToLogin() {
      $state.go('home');
    }

    function authError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error en el Registro',
        template: 'Registro Inválido',
        cssClass: 'text-center',
      });
    }

  }

})();
