(function () {
  'use strict';

  angular
    .module('app.client')
    .service('clientService', clientService);

  /* @ngInject */
  function clientService($http,
                         $base64,
                         sessionService,
                         CORE) {
    var service = {
      url: CORE.API_URL,
      createClient: createClient,
    };

    return service;

    function createClient(client) {
      var post = {
        method: 'POST',
        url: service.url + 'client',
        headers: {
          'Content-Type': 'application/json',
        },
        data: client,
      };
      return $http(post);
    }
  }

})();
